package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.StudentListModel;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.StuddentListViewHolder>{
    private List<StudentListModel> studentList;
    private Context studentListContext;

    public StudentListAdapter(Context studentListContext, List<StudentListModel> studentList){

        this.studentListContext = studentListContext;
        this.studentList = studentList;

    }

    @Override
    public StuddentListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(studentListContext);
        View view = inflater.inflate(R.layout.single_student_list, null);
        return new StudentListAdapter.StuddentListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StuddentListViewHolder holder, int position) {

        StudentListModel studentListModel = studentList.get(position);

        holder.studentName.setText(""+ studentListModel.getStudentName() + "");
        holder.studentId.setText("ID: " + studentListModel.getStudentID()+ "");
        holder.studentContact.setText("Phone: " + studentListModel.getStudentContact()+ "");
        holder.studentClass.setText("Class: " + studentListModel.getStudentClass()+ "");
        holder.studentSection.setText("Section: " + studentListModel.getStudentSection()+ "");
        holder.studentRoll.setText("Roll: " + studentListModel.getStudentRoll()+ "");


    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }


    public class StuddentListViewHolder extends RecyclerView.ViewHolder{

        TextView studentName, studentId, studentContact, studentClass, studentSection, studentRoll;

        public StuddentListViewHolder(View itemView) {
            super(itemView);

            studentName = itemView.findViewById(R.id.student_name);
            studentId = itemView.findViewById(R.id.student_id);
            studentContact = itemView.findViewById(R.id.student_contact);
            studentClass = itemView.findViewById(R.id.student_class);
            studentSection = itemView.findViewById(R.id.student_section);
            studentRoll = itemView.findViewById(R.id.student_roll);

        }
    }

}
