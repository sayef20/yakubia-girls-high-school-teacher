package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.MonthlyAttendanceModel;

public class MonthlyAttendanceAdapter extends RecyclerView.Adapter<MonthlyAttendanceAdapter.MonthlyAttendanceViewHolder> {
    private List<MonthlyAttendanceModel> monthlyAttendanceList;
    private Context monthlyAttendanceContext;

    public MonthlyAttendanceAdapter(Context monthlyAttendanceContext, List<MonthlyAttendanceModel> monthlyAttendanceList){

        this.monthlyAttendanceContext = monthlyAttendanceContext;
        this.monthlyAttendanceList = monthlyAttendanceList;
    }

    @Override
    public MonthlyAttendanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(monthlyAttendanceContext);
        View monthlyAttendanceView = inflater.inflate(R.layout.single_monthly_attendance, null);
        return new MonthlyAttendanceAdapter.MonthlyAttendanceViewHolder(monthlyAttendanceView);
    }

    @Override
    public void onBindViewHolder(MonthlyAttendanceViewHolder holder, int position) {

        final MonthlyAttendanceModel monthlyAttendanceModel = monthlyAttendanceList.get(position);

        holder.classname.setText("Class: " + monthlyAttendanceModel.getClassName() + "");
        holder.totalWorkingDays.setText("Working Days: " + monthlyAttendanceModel.getMonthlyWorkingDays() + "");
        holder.totalStudent.setText("Total: " + monthlyAttendanceModel.getTotalStudent() + "");
        holder.totalPresent.setText("Present: " + monthlyAttendanceModel.getPresentStudent() + "");
        holder.totalAbsent.setText("Absent: " + monthlyAttendanceModel.getAbsentStudent() + "");

    }

    @Override
    public int getItemCount() {
        return monthlyAttendanceList.size();
    }


    public class MonthlyAttendanceViewHolder extends RecyclerView.ViewHolder{

        TextView classname, totalWorkingDays, totalStudent, totalPresent, totalAbsent;

        public MonthlyAttendanceViewHolder(View itemView) {
            super(itemView);

            classname = itemView.findViewById(R.id.monthly_class_name);
            totalWorkingDays = itemView.findViewById(R.id.total_monthly_working_days);
            totalStudent = itemView.findViewById(R.id.monthly_total_student_number);
            totalPresent = itemView.findViewById(R.id.monthly_present_number);
            totalAbsent = itemView.findViewById(R.id.monthly_absent_number);
        }
    }

}

