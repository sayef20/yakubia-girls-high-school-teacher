package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.AdmissionTestModel;

public class AdmissionTestAdapter extends RecyclerView.Adapter<AdmissionTestAdapter.AdmissiontestViewHolder>{

    private List<AdmissionTestModel> admissiontestResultList;
    private Context admissiontestResultContext;

    public AdmissionTestAdapter(Context admissiontestResultContext, List<AdmissionTestModel> admissiontestResultList){

        this.admissiontestResultContext = admissiontestResultContext;
        this.admissiontestResultList = admissiontestResultList;
    }

    @Override
    public AdmissiontestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(admissiontestResultContext);
        View admissiontestResultView = inflater.inflate(R.layout.single_admission_test_result, parent, false);
        return new AdmissionTestAdapter.AdmissiontestViewHolder(admissiontestResultView);
    }

    @Override
    public void onBindViewHolder(AdmissiontestViewHolder holder, int position) {

        final AdmissionTestModel admissionTestModel = admissiontestResultList.get(position);

        holder.className.setText("Class: " + admissionTestModel.getClassName() + "");

        Glide.with(admissiontestResultContext).load(admissionTestModel.getAdmissionTestImgUrl()).into(holder.admissiontestResultimg);


    }

    @Override
    public int getItemCount() {
        return admissiontestResultList.size();
    }


    public class AdmissiontestViewHolder extends RecyclerView.ViewHolder{

        ImageView admissiontestResultimg;
        TextView className;

        public AdmissiontestViewHolder(View itemView) {
            super(itemView);

            admissiontestResultimg = itemView.findViewById(R.id.admission_test_result_img);
            className = itemView.findViewById(R.id.class_name);
        }
    }
}
