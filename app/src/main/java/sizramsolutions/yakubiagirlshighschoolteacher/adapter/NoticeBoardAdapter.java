package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.NoticeDetailsActivity;
import sizramsolutions.yakubiagirlshighschoolteacher.model.NoticeBoardModel;

public class NoticeBoardAdapter extends RecyclerView.Adapter<NoticeBoardAdapter.NoticeBoardViewHolder>{

    private List<NoticeBoardModel> noticeList;
    private Context noticeboardContext;

    public NoticeBoardAdapter(Context noticeboardContext, List<NoticeBoardModel> noticeList){

        this.noticeboardContext = noticeboardContext;
        this.noticeList = noticeList;
    }

    @Override
    public NoticeBoardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(noticeboardContext);
        View noticeListView = inflater.inflate(R.layout.single_notice, parent, false);
        return new NoticeBoardViewHolder(noticeListView);
    }

    @Override
    public void onBindViewHolder(NoticeBoardViewHolder holder, int position) {

        final NoticeBoardModel noticeBoardModel = noticeList.get(position);

        View noticeListView = holder.itemView;

        holder.notice.setText("" + noticeBoardModel.getNotice() + "");
        holder.noticeDate.setText("Date: " + noticeBoardModel.getNoticeDate() + "");

        noticeListView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(noticeboardContext, NoticeDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("home_contentid", noticeBoardModel.getNoticeId());

                noticeboardContext.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public class NoticeBoardViewHolder extends RecyclerView.ViewHolder {

        TextView notice, noticeDate;

        public NoticeBoardViewHolder(View view) {
            super(view);

            notice = view.findViewById(R.id.notice);
            noticeDate = view.findViewById(R.id.notice_date);
        }
    }

}
