package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.FeesModel;

public class FeesAdapter extends RecyclerView.Adapter<FeesAdapter.FeesViewHolder> {

    private List<FeesModel> feesList;
    private Context feesContext;

    public FeesAdapter(List<FeesModel> feesList, Context feesContext){
        this.feesContext = feesContext;
        this.feesList = feesList;
    }

    @Override
    public FeesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(feesContext);
        View view = inflater.inflate(R.layout.single_fees_row, null);
        return new FeesAdapter.FeesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeesViewHolder holder, int position) {

        FeesModel feesModel = feesList.get(position);

        holder.feeCategory.setText(""+ feesModel.getFeeCategory() +"");
        holder.first.setText("" + feesModel.getFirst() + " Tk.");
        holder.second.setText("" + feesModel.getSecond() + " Tk.");
        holder.third.setText("" + feesModel.getThird() + " Tk.");
        holder.fourth.setText("" + feesModel.getFourth() + " Tk.");
    }

    @Override
    public int getItemCount() {
        return feesList.size();
    }


    class FeesViewHolder extends RecyclerView.ViewHolder{

        TextView feeCategory, first, second, third, fourth;

        public FeesViewHolder(View itemView) {
            super(itemView);

            feeCategory = itemView.findViewById(R.id.fee_category);
            first = itemView.findViewById(R.id.first);
            second = itemView.findViewById(R.id.second);
            third = itemView.findViewById(R.id.third);
            fourth = itemView.findViewById(R.id.fourth);
        }
    }

}
