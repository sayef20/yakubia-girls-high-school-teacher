package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.ScholarshipResultModel;

public class ScholarshipResultAdapter extends RecyclerView.Adapter<ScholarshipResultAdapter.ScholarshipResultViewHolder>{

    private List<ScholarshipResultModel> scholarshipResultList;
    private Context scholarshipResultContext;


    public ScholarshipResultAdapter(Context scholarshipResultContext, List<ScholarshipResultModel> scholarshipResultList){

        this.scholarshipResultContext = scholarshipResultContext;
        this.scholarshipResultList = scholarshipResultList;
    }

    @Override
    public ScholarshipResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(scholarshipResultContext);
        View scholarshipResultListView = inflater.inflate(R.layout.single_scholarship_result, parent, false);
        return new ScholarshipResultAdapter.ScholarshipResultViewHolder(scholarshipResultListView);
    }

    @Override
    public void onBindViewHolder(ScholarshipResultViewHolder holder, int position) {

        final ScholarshipResultModel scholarshipResultModel = scholarshipResultList.get(position);


        holder.scholarStudentName.setText(""+ scholarshipResultModel.getScholarStudentName() + "");
        holder.scholarStudentId.setText("Roll: "+ scholarshipResultModel.getScholarStudentId() + "");
        holder.scholarStudentDept.setText("Division: "+ scholarshipResultModel.getScholarStudentDept() + "");

    }

    @Override
    public int getItemCount() {
        return scholarshipResultList.size();
    }


    public class ScholarshipResultViewHolder extends RecyclerView.ViewHolder{

        TextView scholarStudentName, scholarStudentId, scholarStudentDept;

        public ScholarshipResultViewHolder(View view) {
            super(view);

            scholarStudentName = view.findViewById(R.id.scholar_student_name);
            scholarStudentId = view.findViewById(R.id.scholar_student_id);
            scholarStudentDept = view.findViewById(R.id.scholar_student_dept);
        }
    }
}
