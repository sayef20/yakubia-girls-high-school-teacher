package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.ExamScheduleModel;

public class ExamScheduleAdapter extends RecyclerView.Adapter<ExamScheduleAdapter.ExamScheduleViewHolder>{
    private ProgressBar mProgressBar;
    private List<ExamScheduleModel> examScheduleList;
    private Context examScheduleContext;

    public ExamScheduleAdapter(Context examScheduleContext, List<ExamScheduleModel> examScheduleList){
        this.examScheduleContext = examScheduleContext;
        this.examScheduleList = examScheduleList;

    }

    @Override
    public ExamScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(examScheduleContext);
        View examScheduleView = inflater.inflate(R.layout.single_exam_schedule, parent, false);
        return new ExamScheduleAdapter.ExamScheduleViewHolder(examScheduleView);
    }

    @Override
    public void onBindViewHolder(ExamScheduleViewHolder holder, int position) {

        final ExamScheduleModel examScheduleModel = examScheduleList.get(position);
        mProgressBar = new ProgressBar(examScheduleContext, null, android.R.attr.progressBarStyleLarge);
        mProgressBar.setIndeterminate(true);

        View examScheduleView = holder.itemView;

        Glide.with(examScheduleContext).load(examScheduleModel.getScheduleImageUrl()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.examScheduleImage);

        examScheduleView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {




            }
        });



    }

    @Override
    public int getItemCount() {
        return examScheduleList.size();
    }


    public class ExamScheduleViewHolder extends RecyclerView.ViewHolder{

        ImageView examScheduleImage;

        public ExamScheduleViewHolder(View itemView) {
            super(itemView);

            examScheduleImage = itemView.findViewById(R.id.exam_schedule_img);
        }
    }
}
