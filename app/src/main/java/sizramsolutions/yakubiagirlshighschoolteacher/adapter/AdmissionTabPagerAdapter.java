package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.yakubiagirlshighschoolteacher.fragment.AdmissionFeesFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.AdmissionInfoFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.OnlineApplicationFragment;

import static sizramsolutions.yakubiagirlshighschoolteacher.activity.AdmissionActivity.tab;

public class AdmissionTabPagerAdapter extends FragmentPagerAdapter {

    public AdmissionTabPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new AdmissionInfoFragment();
            case 1:
                return new AdmissionFeesFragment();
            case 2:
                return new OnlineApplicationFragment();


        }
        return null;
    }

    @Override
    public int getCount() {


        return tab;
    }

    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Information";
            case 1:
                return "Fees";
            case 2:
                return "Apply";

        }

        return null;
    }
}
