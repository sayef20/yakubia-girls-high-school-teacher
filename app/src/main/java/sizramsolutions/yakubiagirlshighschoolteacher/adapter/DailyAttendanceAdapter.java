package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.DailyAttendanceModel;


public class DailyAttendanceAdapter extends RecyclerView.Adapter<DailyAttendanceAdapter.DailyAttendanceViewHolder>{

    private List<DailyAttendanceModel> dailyAttendanceList;
    private Context dailyAttendanceContext;

    public DailyAttendanceAdapter(Context dailyAttendanceContext, List<DailyAttendanceModel> dailyAttendanceList){

        this.dailyAttendanceContext = dailyAttendanceContext;
        this.dailyAttendanceList = dailyAttendanceList;
    }

    @Override
    public DailyAttendanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(dailyAttendanceContext);
        View dailyAttendanceView = inflater.inflate(R.layout.single_daily_attendance, null);
        return new DailyAttendanceAdapter.DailyAttendanceViewHolder(dailyAttendanceView);
    }

    @Override
    public void onBindViewHolder(DailyAttendanceViewHolder holder, int position) {

        final DailyAttendanceModel dailyAttendanceModel = dailyAttendanceList.get(position);

        holder.className.setText("Class: " + dailyAttendanceModel.getClassName() + "");
        holder.totalStudentNumber.setText("Total: " + dailyAttendanceModel.getTotalStudent() + "");
        holder.presentStudentNumber.setText("Present: " + dailyAttendanceModel.getPresentStudent() + "");
        holder.absentStudentNumber.setText("Absent: " + dailyAttendanceModel.getAbsentStudent() + "");



    }

    @Override
    public int getItemCount() {
        return dailyAttendanceList.size();
    }


    public class DailyAttendanceViewHolder extends RecyclerView.ViewHolder{

        TextView className, totalStudentNumber, presentStudentNumber, absentStudentNumber;


        public DailyAttendanceViewHolder(View itemView) {
            super(itemView);

            className = itemView.findViewById(R.id.class_name);
            totalStudentNumber = itemView.findViewById(R.id.total_student_number);
            presentStudentNumber = itemView.findViewById(R.id.present_number);
            absentStudentNumber = itemView.findViewById(R.id.absent_number);
        }
    }
}
