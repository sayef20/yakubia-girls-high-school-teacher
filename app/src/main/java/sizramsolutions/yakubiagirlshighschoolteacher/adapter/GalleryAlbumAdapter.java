package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.GalleryActivity;
import sizramsolutions.yakubiagirlshighschoolteacher.model.GalleryAlbumModel;

public class GalleryAlbumAdapter extends RecyclerView.Adapter<GalleryAlbumAdapter.GalleryAlbumViewHolder>{

    private Context galleryAlbumContext;
    private List<GalleryAlbumModel> galleryAlbumList;

    public GalleryAlbumAdapter(Context galleryAlbumContext, List<GalleryAlbumModel> galleryAlbumList){
        this.galleryAlbumContext = galleryAlbumContext;
        this.galleryAlbumList = galleryAlbumList;

    }

    @Override
    public GalleryAlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(galleryAlbumContext);
        View galleryAlbumView = inflater.inflate(R.layout.single_gallery_album, parent, false);
        return new GalleryAlbumViewHolder(galleryAlbumView);
    }

    @Override
    public void onBindViewHolder(GalleryAlbumViewHolder holder, int position) {

        View galleryAlbumView = holder.itemView;

        final GalleryAlbumModel galleryAlbumModel = galleryAlbumList.get(position);

        Glide.with(galleryAlbumContext).load(galleryAlbumModel.getAlbumThumbnail()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.singleThumbnail);

        holder.albumName.setText("" + galleryAlbumModel.getAlbumName() + "");


        galleryAlbumView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(galleryAlbumContext.getApplicationContext(), GalleryActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("photo_albumid", galleryAlbumModel.getAlbumId());

                galleryAlbumContext.getApplicationContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return galleryAlbumList.size();
    }


    public class GalleryAlbumViewHolder extends RecyclerView.ViewHolder{

        ImageView singleThumbnail;
        TextView albumName;

        public GalleryAlbumViewHolder(View itemView) {
            super(itemView);

            singleThumbnail = itemView.findViewById(R.id.single_thumbnail);

            albumName = itemView.findViewById(R.id.album_name);

        }
    }
}
