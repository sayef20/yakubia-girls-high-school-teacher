package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.YearlyAttendanceModel;

public class YearlyAttendanceAdapter extends RecyclerView.Adapter<YearlyAttendanceAdapter.YearlyAttendanceViewHolder>{

    private List<YearlyAttendanceModel> yearlyAttendanceList;
    private Context yearlyAttendanceContext;

    public YearlyAttendanceAdapter(Context yearlyAttendanceContext, List<YearlyAttendanceModel> yearlyAttendanceList){

        this.yearlyAttendanceContext = yearlyAttendanceContext;
        this.yearlyAttendanceList = yearlyAttendanceList;
    }

    @Override
    public YearlyAttendanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(yearlyAttendanceContext);
        View yearlyAttendanceView = inflater.inflate(R.layout.single_yearly_attendance, null);
        return new YearlyAttendanceAdapter.YearlyAttendanceViewHolder(yearlyAttendanceView);
    }

    @Override
    public void onBindViewHolder(YearlyAttendanceViewHolder holder, int position) {

        final YearlyAttendanceModel yearlyAttendanceModel = yearlyAttendanceList.get(position);

        holder.classname.setText("Class: " + yearlyAttendanceModel.getClassName() + "");
        holder.totalWorkingDays.setText("Working Days: " + yearlyAttendanceModel.getYearlyWorkingDays() + "");
        holder.totalStudent.setText("Total: " + yearlyAttendanceModel.getTotalStudent() + "");
        holder.totalPresent.setText("Present: " + yearlyAttendanceModel.getPresentStudent() + "");
        holder.totalAbsent.setText("Absent: " + yearlyAttendanceModel.getAbsentStudent() + "");


    }

    @Override
    public int getItemCount() {
        return yearlyAttendanceList.size();
    }

    public class YearlyAttendanceViewHolder extends RecyclerView.ViewHolder{

        TextView classname, totalWorkingDays, totalStudent, totalPresent, totalAbsent;

        public YearlyAttendanceViewHolder(View itemView) {
            super(itemView);

            classname = itemView.findViewById(R.id.yearly_class_name);
            totalWorkingDays = itemView.findViewById(R.id.total_yearly_working_days);
            totalStudent = itemView.findViewById(R.id.yearly_total_student_number);
            totalPresent = itemView.findViewById(R.id.yearly_present_number);
            totalAbsent = itemView.findViewById(R.id.yearly_absent_number);
        }
    }


}
