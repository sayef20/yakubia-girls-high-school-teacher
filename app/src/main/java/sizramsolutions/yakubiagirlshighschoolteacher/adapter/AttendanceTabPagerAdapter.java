package sizramsolutions.yakubiagirlshighschoolteacher.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.yakubiagirlshighschoolteacher.fragment.TabDailyAttendanceFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.TabMonthlyAttendanceFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.TabYearlyAttendanceFragment;

import static sizramsolutions.yakubiagirlshighschoolteacher.fragment.AttendanceReportFragment.tab;

public class AttendanceTabPagerAdapter  extends FragmentPagerAdapter{

    public AttendanceTabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new TabDailyAttendanceFragment();
            case 1:
                return new TabMonthlyAttendanceFragment();
            case 2:
                return new TabYearlyAttendanceFragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        return tab;
    }


    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Daily";
            case 1:
                return "Monthly";
            case 2:
                return "Yearly";

        }

        return null;
    }

}
