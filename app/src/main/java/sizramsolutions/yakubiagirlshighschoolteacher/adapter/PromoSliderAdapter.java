package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.PromoFragment;

public class PromoSliderAdapter extends FragmentPagerAdapter{

    String[] slideTitle;

    public static final String StartImageKey = "startImageKey";
    public static final String TitleKey = "titleKey";


    public PromoSliderAdapter(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();
        slideTitle = resources.getStringArray(R.array.title_array);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();

        bundle.putInt(StartImageKey,getSlideImages(position));
        bundle.putString(TitleKey, slideTitle[position]);
        PromoFragment promoFragment = new PromoFragment();
        promoFragment.setArguments(bundle);

        return promoFragment;
    }

    private int getSlideImages(int position){
        int id=0;
        switch (position){
            case 0:
                id = R.drawable.about_slide1;
                break;
            case 1:
                id = R.drawable.about_slide2;
                break;
            case 2:
                id = R.drawable.about_slide3;
                break;
            case 3:
                id = R.drawable.about_slide4;
                break;
            case 4:
                id = R.drawable.about_slide5;
                break;
            case 5:
                id = R.drawable.about_slide6;
                break;
        }
        return id;

    }

    @Override
    public int getCount() {
        return slideTitle.length;
    }
}
