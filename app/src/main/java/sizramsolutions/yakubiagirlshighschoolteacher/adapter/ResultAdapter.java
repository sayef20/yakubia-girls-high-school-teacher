package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.ResultModel;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder>{

    private List<ResultModel> resultList;
    private Context resultContext;

    public ResultAdapter(Context resultContext, List<ResultModel> resultList){

        this.resultContext = resultContext;
        this.resultList = resultList;
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(resultContext);
        View resultView = inflater.inflate(R.layout.single_result, null);
        return new ResultAdapter.ResultViewHolder(resultView);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        final ResultModel resultModel = resultList.get(position);

        holder.studentName.setText("" + resultModel.getStudentName() + "");
        holder.studentId.setText("ID: " + resultModel.getStudentId() + "");
        holder.studentGpa.setText("GPA: " + resultModel.getStudentGpa() + "");
        holder.studentGrade.setText("Grade: " + resultModel.getStudentGrade() + "");
        holder.studentMark.setText("Marks: " + resultModel.getStudentMark() + "");
        holder.studentPosition.setText("Position: " + resultModel.getStudentPosition() + "");

    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }


    public class ResultViewHolder extends RecyclerView.ViewHolder{

        TextView studentName, studentId, studentGpa, studentGrade, studentMark, studentPosition;

        public ResultViewHolder(View itemView) {
            super(itemView);

            studentName = itemView.findViewById(R.id.result_student_name);
            studentId = itemView.findViewById(R.id.result_student_id);
            studentGpa = itemView.findViewById(R.id.result_student_gpa);
            studentGrade = itemView.findViewById(R.id.result_student_lettergrade);
            studentMark = itemView.findViewById(R.id.result_student_marks);
            studentPosition = itemView.findViewById(R.id.result_student_position);

        }

    }


}
