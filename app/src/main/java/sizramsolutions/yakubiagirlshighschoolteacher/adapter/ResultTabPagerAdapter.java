package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sizramsolutions.yakubiagirlshighschoolteacher.fragment.AdmissiontestFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.ScholarshipResultFragment;

import static sizramsolutions.yakubiagirlshighschoolteacher.activity.ResultActivity.tab;


public class ResultTabPagerAdapter extends FragmentPagerAdapter {


    public ResultTabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new AdmissiontestFragment();
            case 1:
                return new ScholarshipResultFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return tab;
    }



    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "Admission Test";
            case 1:
                return "Scholarship";
        }

        return null;
    }
}
