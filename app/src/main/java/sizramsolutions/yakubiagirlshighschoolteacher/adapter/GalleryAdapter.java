package sizramsolutions.yakubiagirlshighschoolteacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.model.GalleryModel;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private Context galleryContext;
    private List<GalleryModel> galleryList;


    public GalleryAdapter(Context galleryContext, List<GalleryModel> galleryList){
        this.galleryContext = galleryContext;
        this.galleryList = galleryList;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View galleryView = inflater.inflate(R.layout.single_gallery_image, parent, false);
        return new GalleryAdapter.GalleryViewHolder(galleryView);
    }

    @Override
    public void onBindViewHolder(final GalleryViewHolder holder, int position) {

        final GalleryModel galleryModel = galleryList.get(position);

        RelativeLayout layout = new RelativeLayout(galleryContext);
        final ProgressBar progressBar = new ProgressBar(galleryContext, null, android.R.attr.progressBarStyleSmall);
        layout.addView(progressBar);
        progressBar.setIndeterminate(true);


        View galleryView = holder.itemView;


        Glide.with(galleryContext).load(galleryModel.getGalleryThumbnail()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageThumbnail);

    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }



    public class GalleryViewHolder extends RecyclerView.ViewHolder{

        ImageView imageThumbnail;

        public GalleryViewHolder(View itemView) {
            super(itemView);

            imageThumbnail = itemView.findViewById(R.id.single_image_thumbnail);

        }
    }
}