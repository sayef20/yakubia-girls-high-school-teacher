package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import sizramsolutions.yakubiagirlshighschoolteacher.R;


public class PromoFragment extends Fragment {

    public static final String startImageKey = "startImageKey";
    public static final String titleKey = "titleKey";


    public PromoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promo, container, false);

        Bundle bundle = getArguments();
        if(bundle != null){

            int slideImage = bundle.getInt(startImageKey);
            String slideTitle = bundle.getString(titleKey);

            setValues(view, slideImage, slideTitle);
        }

        return view;
    }

    private void setValues(View view, int slideImage, String slideTitle) {
        ImageView imageview = view.findViewById(R.id.start_slider_image);
        imageview.setImageResource(slideImage);

        TextView titleText = view.findViewById(R.id.title_text);
        titleText.setText(slideTitle);


    }

}
