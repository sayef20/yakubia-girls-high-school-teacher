package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.MonthlyAttendanceAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.MonthlyAttendanceModel;


public class TabMonthlyAttendanceFragment extends Fragment {

    private List<MonthlyAttendanceModel> monthlyAttendanceList = new ArrayList<>();

    public TabMonthlyAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_monthly_attendance, container, false);

        RecyclerView monthlyAttendanceRecycler = view.findViewById(R.id.monthly_attendance_list);
        monthlyAttendanceRecycler.setHasFixedSize(true);
        monthlyAttendanceRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        MonthlyAttendanceAdapter monthlyAttendanceAdapter = new MonthlyAttendanceAdapter(getContext(), monthlyAttendanceList);
        monthlyAttendanceRecycler.setAdapter(monthlyAttendanceAdapter);

        monthlyAttendanceData();

        return view;
    }

    private void monthlyAttendanceData(){

        MonthlyAttendanceModel monthlyAttendanceModel = new MonthlyAttendanceModel("One",24, 80, 70, 10);
        monthlyAttendanceList.add(monthlyAttendanceModel);

        monthlyAttendanceModel = new MonthlyAttendanceModel("One",24, 80, 70, 10);
        monthlyAttendanceList.add(monthlyAttendanceModel);

        monthlyAttendanceModel = new MonthlyAttendanceModel("One",24, 80, 70, 10);
        monthlyAttendanceList.add(monthlyAttendanceModel);

        monthlyAttendanceModel = new MonthlyAttendanceModel("One",24, 80, 70, 10);
        monthlyAttendanceList.add(monthlyAttendanceModel);

        monthlyAttendanceModel = new MonthlyAttendanceModel("One",24, 80, 70, 10);
        monthlyAttendanceList.add(monthlyAttendanceModel);

    }

}
