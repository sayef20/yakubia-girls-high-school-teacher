package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.DailyAttendanceAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.DailyAttendanceModel;


public class TabDailyAttendanceFragment extends Fragment {

    private List<DailyAttendanceModel> dailyAttendanceList = new ArrayList<>();
    private DailyAttendanceAdapter dailyAttendanceAdapter;


    public TabDailyAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_daily_attendance, container, false);

        RecyclerView dailyAttendanceRecycler = view.findViewById(R.id.daily_attendance_list);
        dailyAttendanceRecycler.setHasFixedSize(true);
        dailyAttendanceRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        dailyAttendanceAdapter = new DailyAttendanceAdapter(getContext(), dailyAttendanceList);
        dailyAttendanceRecycler.setAdapter(dailyAttendanceAdapter);

        dailyAttendanceData();

        return view;
    }


    private void dailyAttendanceData(){

        DailyAttendanceModel dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);

        dailyAttendanceModel = new DailyAttendanceModel("One", 80, 70, 10);
        dailyAttendanceList.add(dailyAttendanceModel);
    }

}
