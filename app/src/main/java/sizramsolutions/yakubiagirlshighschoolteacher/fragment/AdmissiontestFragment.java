package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.AdmissionTestAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.AdmissionTestModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class AdmissiontestFragment extends Fragment{

    private static final String TAG = AdmissiontestFragment.class.getSimpleName();

    private List<AdmissionTestModel> admissiontestResultList = new ArrayList<>();

    RequestQueue requestQueue;

    private static final String URL_ADMISSIONTEST_RESULT = "http://yakubia.edu.bd/index.php/api/admission_details/";

    Spinner spinner;

    public AdmissiontestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admissiontest, container, false);

        RecyclerView admissiontestResultRecycler = view.findViewById(R.id.admission_test_result_list);
        admissiontestResultRecycler.setHasFixedSize(true);
        admissiontestResultRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        admissiontestResultRecycler.setItemAnimator(new DefaultItemAnimator());

        AdmissionTestAdapter admissiontestResultAdapter = new AdmissionTestAdapter(getContext(), admissiontestResultList);
        admissiontestResultRecycler.setAdapter(admissiontestResultAdapter);

        spinner = view.findViewById(R.id.admission_class_spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(),R.array.admission__test_class_list, R.layout.spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(id > 0){
                    admissionTestResultData(id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }


    private void admissionTestResultData(long id){

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();

        StringRequest stringRequest = new StringRequest(URL_ADMISSIONTEST_RESULT+id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    AdmissionTestModel admissionTestModel = new AdmissionTestModel(

                            jsonObject.optString("admission_test_result_file", "not found"),
                            jsonObject.optString("class", "not found")
                    );

                    admissiontestResultList.add(admissionTestModel);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);

    }

}
