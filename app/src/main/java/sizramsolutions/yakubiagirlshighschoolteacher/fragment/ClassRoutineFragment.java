package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.MainActivity;


public class ClassRoutineFragment extends Fragment {


    public ClassRoutineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_class_routine, container, false);
    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((MainActivity) getActivity())
                .setActionBarTitle("Class Routine");
        super.onResume();
    }

}
