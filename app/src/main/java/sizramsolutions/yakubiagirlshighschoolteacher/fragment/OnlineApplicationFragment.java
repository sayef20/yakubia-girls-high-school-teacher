package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.app.DownloadManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import sizramsolutions.yakubiagirlshighschoolteacher.R;


public class OnlineApplicationFragment extends Fragment {

    Button downloadFormButton;


    public OnlineApplicationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_online_application, container, false);
        downloadFormButton = view.findViewById(R.id.download_form_btn);

        downloadFormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(), "Download Form is not Available Right Now!", Toast.LENGTH_LONG).show();


            }
        });
        return view;
    }

}
