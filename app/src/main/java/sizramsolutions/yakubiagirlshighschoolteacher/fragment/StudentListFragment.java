package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.MainActivity;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.StudentListAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.StudentListModel;


public class StudentListFragment extends Fragment {

    private List<StudentListModel> studentList = new ArrayList<>();
    private StudentListAdapter studentListAdapter;

    public StudentListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_list, container, false);

        RecyclerView studentListRecycler = view.findViewById(R.id.student_list);
        studentListRecycler.setHasFixedSize(true);
        studentListRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        studentListAdapter =new StudentListAdapter(getContext(), studentList);
        studentListRecycler.setAdapter(studentListAdapter);


        studentListData();

        return view;
    }


    private void studentListData(){
        StudentListModel studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
        studentListModel = new StudentListModel("Anowar Sayef", "16-99369-2", "0167890654", "One", "A", 65);
        studentList.add(studentListModel);
    }


    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((MainActivity) getActivity())
                .setActionBarTitle("Student List");
        super.onResume();
    }

}
