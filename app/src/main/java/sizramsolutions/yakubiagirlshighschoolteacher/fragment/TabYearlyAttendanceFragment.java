package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.YearlyAttendanceAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.YearlyAttendanceModel;

public class TabYearlyAttendanceFragment extends Fragment {

    private List<YearlyAttendanceModel> yearlyAttendanceList = new ArrayList<>();

    public TabYearlyAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_yearly_attendance, container, false);

        RecyclerView yearlyAttendanceRecycler = view.findViewById(R.id.yearly_attendance_list);
        yearlyAttendanceRecycler.setHasFixedSize(true);
        yearlyAttendanceRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        YearlyAttendanceAdapter yearlyAttendanceAdapter = new YearlyAttendanceAdapter(getContext(), yearlyAttendanceList );
        yearlyAttendanceRecycler.setAdapter(yearlyAttendanceAdapter);

        yearlyAttendanceData();

        return view;
    }

    private void yearlyAttendanceData(){

        YearlyAttendanceModel yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);

        yearlyAttendanceModel = new YearlyAttendanceModel("One",24, 80, 70, 10);
        yearlyAttendanceList.add(yearlyAttendanceModel);
    }


}
