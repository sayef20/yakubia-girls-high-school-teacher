package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.MainActivity;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.AttendanceTabPagerAdapter;


public class AttendanceReportFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int tab= 3;

    public AttendanceReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_attendance_report,null);
        tabLayout= v.findViewById(R.id.tabs);
        viewPager= v.findViewById(R.id.viewpager);

        //set an adpater
        viewPager.setAdapter(new AttendanceTabPagerAdapter( getChildFragmentManager()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });


        return v;

    }

    // Adding Custom fragment action bar Title //
    @Override
    public void onResume() {
        ((MainActivity) getActivity())
                .setActionBarTitle("Attendance Report");
        super.onResume();
    }

}
