package sizramsolutions.yakubiagirlshighschoolteacher.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.activity.NoticeBoardActivity;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.ScholarshipResultAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.ScholarshipResultModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class ScholarshipResultFragment extends Fragment {

    private static final String TAG = NoticeBoardActivity.class.getSimpleName();

    private List<ScholarshipResultModel> scholarshipResultList = new ArrayList<>();
    private ScholarshipResultAdapter scholarshipResultAdapter;

    RequestQueue requestQueue;


    private static final String URL_SCHOLARSHIP_RESULT = "http://yakubia.edu.bd/index.php/api/scholarship_details/";

    Spinner spinner;

    public ScholarshipResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scholarshiptest, container, false);


        spinner =(Spinner) view.findViewById(R.id.scholarship_spinner);

        RecyclerView scholarshipResultRecycler = (RecyclerView)view.findViewById(R.id.scholarship_result_list);
        scholarshipResultRecycler.setHasFixedSize(true);
        scholarshipResultRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        scholarshipResultAdapter =new ScholarshipResultAdapter(getContext(), scholarshipResultList);
        scholarshipResultRecycler.setAdapter(scholarshipResultAdapter);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(),R.array.scholarship_array, R.layout.spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(id > 0){
                    scholarshipResultData(id);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        return view;
    }


    private void scholarshipResultData(long id){

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getContext()).getRequestQueue();
        scholarshipResultList.clear();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_SCHOLARSHIP_RESULT+id, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray schoralshipResultArray) {

                for(int i = 0; i < schoralshipResultArray.length(); i++){

                    try{

                        JSONObject scholarshipResultObject = schoralshipResultArray.getJSONObject(i);

                        ScholarshipResultModel scholarshipResultModel = new ScholarshipResultModel(

                                scholarshipResultObject.optString("student_name","Not Found"),
                                scholarshipResultObject.optString("scholarship_result_id","Not Found"),
                                scholarshipResultObject.optString("division","Not Found"),
                                scholarshipResultObject.optInt("", 0)
                        );

                        scholarshipResultList.add(scholarshipResultModel);


                    } catch (JSONException e) {
                        e.printStackTrace();



                    }finally {
                        scholarshipResultAdapter.notifyDataSetChanged();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "" + error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(jsonArrayRequest);
    }



}
