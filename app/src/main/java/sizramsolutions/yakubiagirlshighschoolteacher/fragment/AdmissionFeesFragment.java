package sizramsolutions.yakubiagirlshighschoolteacher.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.FeesAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.FeesModel;

public class AdmissionFeesFragment extends Fragment {

    private List<FeesModel> feesList = new ArrayList<>();
    private FeesAdapter feesListAdapter;


    public AdmissionFeesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_admission_fees, container, false);

        RecyclerView monthlyFeesRecycler = (RecyclerView)view.findViewById(R.id.monthly_fees_list);
        //RecyclerView yearlyFeesRecycler = (RecyclerView)view.findViewById(R.id.yearly_fees_list);
        monthlyFeesRecycler.setHasFixedSize(true);
        //yearlyFeesRecycler.setHasFixedSize(true);

        monthlyFeesRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
       // yearlyFeesRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        feesListAdapter = new FeesAdapter(feesList, getContext());

        monthlyFeesRecycler.setAdapter(feesListAdapter);
        //yearlyFeesRecycler.setAdapter(feesListAdapter);

        monthlyFeesData();

        //yearlyFeesData();

        return view;
    }


    private void monthlyFeesData(){
        FeesModel feesModel = new FeesModel("বেতন", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("টিফিন", 190, 190, 190 , 190);
        feesList.add(feesModel);
        feesModel = new FeesModel("বিদ্যুৎ", 35, 35, 35 , 35);
        feesList.add(feesModel);
        feesModel = new FeesModel("গ্রাচুয়িটি", 35, 35, 35 , 35);
        feesList.add(feesModel);
        feesModel = new FeesModel("পানি", 5, 5, 5 , 5);
        feesList.add(feesModel);
        feesModel = new FeesModel("কম্পিউটার", 0, 10, 15 , 20);
        feesList.add(feesModel);
        feesModel = new FeesModel("শিক্ষা উন্নয়ন", 30, 20, 20 , 20);
        feesList.add(feesModel);
        feesModel = new FeesModel("বিবিধ", 35, 35, 35 , 35);
        feesList.add(feesModel);
        feesModel = new FeesModel("অটোমেশন", 20, 20, 20 , 20);
        feesList.add(feesModel);
        feesModel = new FeesModel("গ্যস বিল", 5, 5, 5 , 5);
        feesList.add(feesModel);
        feesModel = new FeesModel("সর্বমোট", 515, 505, 510 , 515);
        feesList.add(feesModel);

        feesListAdapter.notifyDataSetChanged();

    }
 /*private void yearlyFeesData(){
        FeesModel feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);
        feesModel = new FeesModel("Tution Fee", 160, 150, 150 , 150);
        feesList.add(feesModel);

        feesListAdapter.notifyDataSetChanged();

    }
*/
}
