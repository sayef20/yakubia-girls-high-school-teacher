package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import sizramsolutions.yakubiagirlshighschoolteacher.R;

public class AboutActivity extends AppCompatActivity {

    ImageView aboutImage;
    TextView aboutHeading, aboutDesc, historyHeading, historyDesc, futureplanHeading, futureplanDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aboutImage = (ImageView)findViewById(R.id.about_image);

        aboutHeading = (TextView)findViewById(R.id.about_heading);
        aboutDesc = (TextView)findViewById(R.id.about_school_desc);
        historyHeading = (TextView)findViewById(R.id.history_heading);
        historyDesc = (TextView)findViewById(R.id.history_desc);
        futureplanHeading = (TextView)findViewById(R.id.future_plan_heading);
        futureplanDesc = (TextView)findViewById(R.id.future_plan_desc);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
    }

    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
