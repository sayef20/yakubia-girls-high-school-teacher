package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;


import sizramsolutions.yakubiagirlshighschoolteacher.R;

public class SplashActivity extends AppCompatActivity {

    TextView splashText, loadingText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashText = (TextView)findViewById(R.id.splash_text);
        loadingText = (TextView)findViewById(R.id.loading_text);

        loadingBlink();


        Handler splashHandler = new Handler();
        splashHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, StartActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2400);
    }


    public void loadingBlink(){

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(600); // manage the time of the blink with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        loadingText.startAnimation(anim);
    }
}

