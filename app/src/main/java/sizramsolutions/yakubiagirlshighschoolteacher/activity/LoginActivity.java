package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import sizramsolutions.yakubiagirlshighschoolteacher.R;

public class LoginActivity extends AppCompatActivity {

    Button login;
    EditText editEmail, editPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = (Button) findViewById(R.id.login_btn);

        editEmail = (EditText) findViewById(R.id.email);
        editPass = (EditText) findViewById(R.id.password);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent method to just to work on dashboard. Later to be replaced //
                Intent intent_main = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent_main);
            }

        });

    }

    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
