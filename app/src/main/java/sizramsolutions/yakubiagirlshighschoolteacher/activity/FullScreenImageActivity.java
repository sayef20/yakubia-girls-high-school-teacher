package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import sizramsolutions.yakubiagirlshighschoolteacher.R;

public class FullScreenImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        ImageButton closeButton = (ImageButton) findViewById(R.id.ib_close);
        String imageZoomUrl =  getIntent().getExtras().getString("photoUrl");
        ImageView fullScreenImageView = (ImageView) findViewById(R.id.fullScreenImageView);
        Glide.with(getApplicationContext()).load(imageZoomUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(fullScreenImageView);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullScreenImageActivity.super.onBackPressed();
                finish();
            }
        });
    }
}
