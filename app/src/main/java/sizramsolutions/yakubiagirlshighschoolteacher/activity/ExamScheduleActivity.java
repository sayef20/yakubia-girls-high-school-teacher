package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.ExamScheduleAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.ExamScheduleModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class ExamScheduleActivity extends AppCompatActivity {

    private static final String TAG = ExamScheduleActivity.class.getSimpleName();

    private List<ExamScheduleModel> examScheduleList = new ArrayList<>();
    private ExamScheduleAdapter examScheduleAdapter;

    String examSchedule;

    RequestQueue requestQueue;

    private static final String URL_EXAM_SCHEDULE = "http://yakubia.edu.bd/index.php/api/exam_routine";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_schedule);

        RecyclerView examScheduleRecycler = (RecyclerView)findViewById(R.id.exam_schedule_list);
        examScheduleRecycler.setHasFixedSize(true);
        examScheduleRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        examScheduleRecycler.setItemAnimator(new DefaultItemAnimator());

        examScheduleAdapter = new ExamScheduleAdapter(getApplicationContext(), examScheduleList);
        examScheduleRecycler.setAdapter(examScheduleAdapter);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        examScheduleData();
    }


    private void examScheduleData(){

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_EXAM_SCHEDULE, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray examScheduleArray) {

               findViewById(R.id.progressBar2).setVisibility(View.GONE);

                for(int i =0; i < examScheduleArray.length(); i++){
                    try {

                        JSONObject examScheduleJSONObject = examScheduleArray.getJSONObject(i);

                        ExamScheduleModel examScheduleModel = new ExamScheduleModel(

                                examSchedule = examScheduleJSONObject.optString("routine_file", "Not Found")

                        );

                        examScheduleList.add(examScheduleModel);

                    } catch (JSONException e){

                        e.printStackTrace();
                    }finally {
                        examScheduleAdapter.notifyDataSetChanged();

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            }


        });

        requestQueue.add(jsonArrayRequest);
    }


    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
