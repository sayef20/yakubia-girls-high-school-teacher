package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.PromoSliderAdapter;

public class StartActivity extends FragmentActivity {

    Toolbar toolbar;
    //Button loginBtn, contactBtn;

    CardView aboutSchool, noticeBoard, admission, examSchedule, results, gallery;

    ViewPager promoSlider;
    PromoSliderAdapter promoSliderAdapter;
    TabLayout promoTabLayout;

    TextView sizramLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        promoSliderAdapter = new PromoSliderAdapter(getSupportFragmentManager(),this);

        promoSlider = findViewById(R.id.start_slider);
        promoTabLayout = findViewById(R.id.tab_layout);

        promoSlider.setAdapter(promoSliderAdapter);
        promoTabLayout.setupWithViewPager(promoSlider);

        toolbar = (Toolbar)findViewById(R.id.start_toolbar);
        /*loginBtn = (Button)findViewById(R.id.goto_login_btn);
        contactBtn = (Button)findViewById(R.id.contact_btn);
*/
        aboutSchool = (CardView)findViewById(R.id.about_school);
        noticeBoard = (CardView)findViewById(R.id.notice_board);
        admission = (CardView)findViewById(R.id.admission);
        examSchedule = (CardView)findViewById(R.id.exam_schedule);
        results = (CardView)findViewById(R.id.results);
        gallery = (CardView)findViewById(R.id.gallery);

        sizramLink = findViewById(R.id.sizram_link);

/*
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });*/

        aboutSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });

        noticeBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, NoticeBoardActivity.class);
                startActivity(intent);
            }
        });

        admission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, AdmissionActivity.class);
                startActivity(intent);
            }
        });

        examSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, ExamScheduleActivity.class);
                startActivity(intent);
            }
        });

        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, ResultActivity.class);
                startActivity(intent);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, GalleryAlbumActivity.class);
                startActivity(intent);
            }
        });

        sizramLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://sizramsolutions.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        disableTabStrip();

    }

    public void disableTabStrip() {
        LinearLayout tabStrip = (LinearLayout) promoTabLayout.getChildAt(0);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }
}
