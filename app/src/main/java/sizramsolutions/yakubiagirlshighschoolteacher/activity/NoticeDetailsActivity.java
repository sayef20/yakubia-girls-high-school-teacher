package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONObject;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class NoticeDetailsActivity extends AppCompatActivity {

    private static final String TAG = NoticeDetailsActivity.class.getSimpleName();

    RequestQueue requestQueue;

    int noticeId;

    ImageView noticeImage;

    String noticeImageLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);

        noticeImage = (ImageView) findViewById(R.id.notice_image);

        noticeId = getIntent().getIntExtra("home_contentid", 70);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }


        noticeDetailsImage();

    }

    private void noticeDetailsImage() {

        String URL_NOTICE_DETAILS = "http://yakubia.edu.bd/index.php/api/notices_details/" + noticeId;

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(URL_NOTICE_DETAILS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject noticeDetailsObject) {

                    noticeImageLink = noticeDetailsObject.optString("file", "Not Found");

                   Glide.with(getApplicationContext()).load(noticeImageLink).diskCacheStrategy(DiskCacheStrategy.ALL).listener(new RequestListener<String, GlideDrawable>() {
                       @Override
                       public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                           findViewById(R.id.progressBar2).setVisibility(View.GONE);
                           return false;
                       }

                       @Override
                       public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                           findViewById(R.id.progressBar2).setVisibility(View.GONE);
                           return false;
                       }
                   }).into(noticeImage);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();


            }
        });

        requestQueue.add(jsonObjectRequest);

    }

    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

}
