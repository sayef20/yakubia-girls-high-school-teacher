package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.GalleryAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.GalleryModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class GalleryActivity extends AppCompatActivity {

    private List<GalleryModel> galleryList = new ArrayList<>();

    RequestQueue requestQueue;

    GalleryAdapter galleryAdapter;

    int albumId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galery);

        albumId = getIntent().getIntExtra("photo_albumid", 9);


        RecyclerView galleryRecycler = (RecyclerView) findViewById(R.id.gallery_image_list);
        galleryRecycler.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        galleryRecycler.setItemAnimator(new DefaultItemAnimator());

        galleryAdapter = new GalleryAdapter(getApplicationContext() ,galleryList);
        galleryRecycler.setAdapter(galleryAdapter);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        galleryData();


        galleryRecycler.addOnItemTouchListener(new RecyclerTouchListener(this,
                galleryRecycler, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                String imageurl=  galleryList.get(position).getGalleryThumbnail().toString();

                Intent intent = new Intent(getApplicationContext(),FullScreenImageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("photoUrl", imageurl);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    private void galleryData(){

        String URL_GALLERY = "http://yakubia.edu.bd/index.php/api/gallery_images/"+albumId;

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_GALLERY, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray galleryArray) {

                for(int i =0; i < galleryArray.length(); i++){

                    try{

                        JSONObject galleryObject = galleryArray.getJSONObject(i);

                        GalleryModel galleryModel = new GalleryModel(

                                galleryObject.optString("photo", "Not Found"),
                                galleryObject.optInt("photoid", 39)

                        );

                        galleryList.add(galleryModel);


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }finally {
                        galleryAdapter.notifyDataSetChanged();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
