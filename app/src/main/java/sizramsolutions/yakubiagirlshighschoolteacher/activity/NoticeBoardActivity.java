package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.NoticeBoardAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.NoticeBoardModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class NoticeBoardActivity extends AppCompatActivity {

    private static final String TAG = NoticeBoardActivity.class.getSimpleName();

    private List<NoticeBoardModel> noticeList = new ArrayList<>();
    private NoticeBoardAdapter noticeBoardAdapter;


    RequestQueue requestQueue;

    private static final String URL_NOTICE = "http://yakubia.edu.bd/index.php/api/notices";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);

        RecyclerView noticeBoardRecycler = (RecyclerView)findViewById(R.id.notice_list);
        noticeBoardRecycler.setHasFixedSize(true);
        noticeBoardRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        noticeBoardAdapter =new NoticeBoardAdapter(getApplicationContext(), noticeList);
        noticeBoardRecycler.setAdapter(noticeBoardAdapter);


        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        noticeBoardData();

    }

    private void noticeBoardData(){
        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_NOTICE, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray noticeArray) {
                findViewById(R.id.progressBar2).setVisibility(View.GONE);

                for(int i =0; i < noticeArray.length(); i++){
                    try{
                        JSONObject noticeJsonObject = noticeArray.getJSONObject(i);

                        NoticeBoardModel noticeBoardModel = new NoticeBoardModel(
                                noticeJsonObject.optInt("home_contentid", 0),
                                noticeJsonObject.optString("home_content_title", "Not Found"),
                                noticeJsonObject.optString("date", "Not Found"),
                                noticeJsonObject.optString("file", "Not Found")
                        );

                        noticeList.add(noticeBoardModel);

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }finally {
                        noticeBoardAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(NoticeBoardActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }

    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
