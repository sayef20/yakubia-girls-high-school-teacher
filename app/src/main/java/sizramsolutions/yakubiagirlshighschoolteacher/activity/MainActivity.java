package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.AttendanceReportFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.ClassRoutineFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.ExamScheduleFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.NoticeFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.ResultFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.StudentListFragment;
import sizramsolutions.yakubiagirlshighschoolteacher.fragment.TopSheetFragment;

public class MainActivity extends AppCompatActivity {

    DrawerLayout drawer;
    NavigationView navigationView;
    FragmentManager FM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FM = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = FM.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, new StudentListFragment()).commit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawer.closeDrawers();
                if (item.getItemId() == R.id.nav_student_list) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new StudentListFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId() == R.id.nav_class_schedule) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new ClassRoutineFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();
                } else if (item.getItemId() == R.id.nav_exam_schedule) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new ExamScheduleFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                } else if (item.getItemId() == R.id.nav_attendance_report) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new AttendanceReportFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                } else if (item.getItemId() == R.id.nav_results) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new ResultFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                }else if (item.getItemId() == R.id.nav_topsheet) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new TopSheetFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                }else if (item.getItemId() == R.id.nav_notice) {
                    FragmentTransaction fragmentTransaction = FM.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, new NoticeFragment());
                    fragmentTransaction.addToBackStack("Dashboard");
                    fragmentTransaction.commit();

                }

                return false;
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

/*        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    logoutUser();
                    return true;
                }
            });

            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }



    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
