package sizramsolutions.yakubiagirlshighschoolteacher.activity;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sizramsolutions.yakubiagirlshighschoolteacher.R;
import sizramsolutions.yakubiagirlshighschoolteacher.adapter.GalleryAlbumAdapter;
import sizramsolutions.yakubiagirlshighschoolteacher.model.GalleryAlbumModel;
import sizramsolutions.yakubiagirlshighschoolteacher.util.AppController;

public class GalleryAlbumActivity extends AppCompatActivity {

    private static final String TAG = GalleryAlbumActivity.class.getSimpleName();

    private List<GalleryAlbumModel> galleryAlbumList = new ArrayList<>();
    private GalleryAlbumAdapter galleryAlbumAdapter;

    String albumThumbnail, albumName;

    RequestQueue requestQueue;

    private static final String URL_GALLERY_ALBUM = "http://yakubia.edu.bd/index.php/api/gallery_albums";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_album);

        RecyclerView galleryAlbumRecycler = (RecyclerView) findViewById(R.id.gallery_album_list);
        galleryAlbumRecycler.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        galleryAlbumRecycler.setItemAnimator(new DefaultItemAnimator());

        galleryAlbumAdapter = new GalleryAlbumAdapter(getApplicationContext() ,galleryAlbumList);
        galleryAlbumRecycler.setAdapter(galleryAlbumAdapter);

        //Adding Back Button
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        galleryAlbumData();

    }


    private void galleryAlbumData() {

        // Fetching JSON Data from API //
        requestQueue = AppController.getInstance(getApplicationContext()).getRequestQueue();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL_GALLERY_ALBUM, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray albumArray) {

                findViewById(R.id.progressBar2).setVisibility(View.GONE);

                for(int i =0; i < albumArray.length(); i++){

                    try{

                        JSONObject albumObject = albumArray.getJSONObject(i);

                        GalleryAlbumModel galleryAlbumModel = new GalleryAlbumModel(

                                albumObject.optInt("photo_albumid", 0),
                                albumObject.optString("cover_photo", "Not Found"),
                                albumObject.optString("album_title","Not Found")
                        );

                        galleryAlbumList.add(galleryAlbumModel);


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }finally {
                        galleryAlbumAdapter.notifyDataSetChanged();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            }
        });

        requestQueue.add(jsonArrayRequest);

    }









    // Action Bar Back Button Method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
