package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class ExamScheduleModel {

    private String url = "http://yakubia.edu.bd/assets/uploads/files/exam_routine/";
    private String imageUrl;

    private String scheduleImageUrl ;

    public ExamScheduleModel(String scheduleImageUrl){
        this.scheduleImageUrl = url + scheduleImageUrl;

    }

    public String getScheduleImageUrl() {

        return scheduleImageUrl;
    }

    public void setScheduleImageUrl(String scheduleImageUrl) {
        this.scheduleImageUrl = scheduleImageUrl;
    }
}
