package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class AdmissionTestModel {

    private String admissionTestImgUrl, className;


    public AdmissionTestModel(String admissionTestImgUrl, String className){

        this.admissionTestImgUrl = admissionTestImgUrl;
        this.className = className;
    }


    public String getAdmissionTestImgUrl() {
        return admissionTestImgUrl;
    }

    public void setAdmissionTestImgUrl(String admissionTestImgUrl) {
        this.admissionTestImgUrl = admissionTestImgUrl;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
