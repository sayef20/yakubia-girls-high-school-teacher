package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class GalleryAlbumModel {

    private int albumId;
    private String albumThumbnail, albumName;

    public GalleryAlbumModel(int albumId, String albumThumbnail, String albumName){
        this.albumId = albumId;
        this.albumThumbnail = albumThumbnail;
        this.albumName = albumName;

    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getAlbumThumbnail() {
        return albumThumbnail;
    }

    public void setAlbumThumbnail(String albumThumbnail) {
        this.albumThumbnail = albumThumbnail;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }
}
