package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class NoticeBoardModel {

    private int noticeId;
    private String notice, noticeDate, noticeImage;

    public NoticeBoardModel(){

    }

    public NoticeBoardModel(int noticeId, String notice, String noticeDate, String noticeImage){
        this.noticeId = noticeId;
        this.notice = notice;
        this.noticeDate = noticeDate;
        this.noticeImage = noticeImage;

    }

    public int getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(int noticeId) {
        this.noticeId = noticeId;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(String noticeDate) {
        this.noticeDate = noticeDate;
    }

    public String getNoticeImage() {
        return noticeImage;
    }

    public void setNoticeImage(String noticeImage) {
        this.noticeImage = noticeImage;
    }
}
