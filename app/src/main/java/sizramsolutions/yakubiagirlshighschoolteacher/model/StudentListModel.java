package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class StudentListModel {

   String studentName, studentID, studentContact, studentClass, studentSection;
    int studentRoll;


    public StudentListModel(String studentName, String studentID, String studentContact, String studentClass, String studentSection, int studentRoll){

        this.studentName = studentName;
        this.studentID = studentID;
        this.studentContact = studentContact;
        this.studentClass = studentClass;
        this.studentSection = studentSection;
        this.studentRoll = studentRoll;

    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentContact() {
        return studentContact;
    }

    public void setStudentContact(String studentContact) {
        this.studentContact = studentContact;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public String getStudentSection() {
        return studentSection;
    }

    public void setStudentSection(String studentSection) {
        this.studentSection = studentSection;
    }

    public int getStudentRoll() {
        return studentRoll;
    }

    public void setStudentRoll(int studentRoll) {
        this.studentRoll = studentRoll;
    }
}
