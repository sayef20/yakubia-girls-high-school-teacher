package sizramsolutions.yakubiagirlshighschoolteacher.model;


public class MonthlyAttendanceModel {
    private String className;
    private int monthlyWorkingDays, totalStudent, presentStudent, absentStudent;

    public MonthlyAttendanceModel(String className, int monthlyWorkingDays, int totalStudent, int presentStudent, int absentStudent){

        this.className = className;
        this.monthlyWorkingDays = monthlyWorkingDays;
        this.totalStudent = totalStudent;
        this.presentStudent = presentStudent;
        this.absentStudent = absentStudent;

    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getMonthlyWorkingDays() {
        return monthlyWorkingDays;
    }

    public void setMonthlyWorkingDays(int monthlyWorkingDays) {
        this.monthlyWorkingDays = monthlyWorkingDays;
    }

    public int getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(int totalStudent) {
        this.totalStudent = totalStudent;
    }

    public int getPresentStudent() {
        return presentStudent;
    }

    public void setPresentStudent(int presentStudent) {
        this.presentStudent = presentStudent;
    }

    public int getAbsentStudent() {
        return absentStudent;
    }

    public void setAbsentStudent(int absentStudent) {
        this.absentStudent = absentStudent;
    }
}
