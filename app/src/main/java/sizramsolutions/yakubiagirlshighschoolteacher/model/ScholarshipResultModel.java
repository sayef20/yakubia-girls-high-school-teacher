package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class ScholarshipResultModel {

    private String scholarStudentName, scholarStudentId, scholarStudentDept;
    private int scholarshipType;

    public ScholarshipResultModel(String scholarStudentName, String scholarStudentId, String scholarStudentDept, int scholarshipType){

        this.scholarStudentName = scholarStudentName;
        this.scholarStudentId = scholarStudentId;
        this.scholarStudentDept = scholarStudentDept;
        this.scholarshipType = scholarshipType;
    }

    public String getScholarStudentName() {
        return scholarStudentName;
    }

    public void setScholarStudentName(String scholarStudentName) {
        this.scholarStudentName = scholarStudentName;
    }

    public String getScholarStudentId() {
        return scholarStudentId;
    }

    public void setScholarStudentId(String scholarStudentId) {
        this.scholarStudentId = scholarStudentId;
    }

    public String getScholarStudentDept() {
        return scholarStudentDept;
    }

    public void setScholarStudentDept(String scholarStudentDept) {
        this.scholarStudentDept = scholarStudentDept;
    }

    public int getScholarshipType() {
        return scholarshipType;
    }

    public void setScholarshipType(int scholarshipType) {
        this.scholarshipType = scholarshipType;
    }
}
