package sizramsolutions.yakubiagirlshighschoolteacher.model;


public class DailyAttendanceModel {

    private String className;
    private int totalStudent, presentStudent, absentStudent;

    public DailyAttendanceModel(String className, int totalStudent, int presentStudent, int absentStudent){
        this.className = className;
        this.totalStudent = totalStudent;
        this.presentStudent = presentStudent;
        this.absentStudent = absentStudent;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(int totalStudent) {
        this.totalStudent = totalStudent;
    }

    public int getPresentStudent() {
        return presentStudent;
    }

    public void setPresentStudent(int presentStudent) {
        this.presentStudent = presentStudent;
    }

    public int getAbsentStudent() {
        return absentStudent;
    }

    public void setAbsentStudent(int absentStudent) {
        this.absentStudent = absentStudent;
    }
}
