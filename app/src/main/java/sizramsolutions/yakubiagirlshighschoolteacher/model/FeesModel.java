package sizramsolutions.yakubiagirlshighschoolteacher.model;


public class FeesModel {

    private String feeCategory;
    private int first, second, third, fourth;

    public FeesModel(){

    }

    public FeesModel(String feeCategory, int first, int second, int third, int fourth){

        this.feeCategory = feeCategory;
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }

    public String getFeeCategory() {
        return feeCategory;
    }

    public void setFeeCategory(String feeCategory) {
        this.feeCategory = feeCategory;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getThird() {
        return third;
    }

    public void setThird(int third) {
        this.third = third;
    }

    public int getFourth() {
        return fourth;
    }

    public void setFourth(int fourth) {
        this.fourth = fourth;
    }
}
