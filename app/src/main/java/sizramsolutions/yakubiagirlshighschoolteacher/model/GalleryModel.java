package sizramsolutions.yakubiagirlshighschoolteacher.model;

public class GalleryModel {

    private String galleryThumbnail;
    private int thumbnailId;

    public GalleryModel(String galleryThumbnail, int thumbnailId){

        this.galleryThumbnail = galleryThumbnail;
        this.thumbnailId = thumbnailId;

    }

    public String getGalleryThumbnail() {
        return galleryThumbnail;
    }

    public void setGalleryThumbnail(String galleryThumbnail) {
        this.galleryThumbnail = galleryThumbnail;
    }

    public int getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(int thumbnailId) {
        this.thumbnailId = thumbnailId;
    }
}
