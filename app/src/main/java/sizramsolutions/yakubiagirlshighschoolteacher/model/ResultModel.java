package sizramsolutions.yakubiagirlshighschoolteacher.model;

import sizramsolutions.yakubiagirlshighschoolteacher.activity.ResultActivity;

public class ResultModel {
    private String studentName, studentId, studentGrade;
    private float studentGpa;
    private int  stdId, studentMark, studentPosition;

    public ResultModel(String studentName, String studentId, String studentGrade, float studentGpa, int studentMark, int studentPosition, int stdId){

        this.stdId = stdId;
        this.studentName = studentName;
        this.studentId = studentId;
        this.studentGrade = studentGrade;
        this.studentGpa = studentGpa;
        this.studentMark = studentMark;
        this.studentPosition = studentPosition;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentGrade() {
        return studentGrade;
    }

    public void setStudentGrade(String studentGrade) {
        this.studentGrade = studentGrade;
    }

    public float getStudentGpa() {
        return studentGpa;
    }

    public void setStudentGpa(float studentGpa) {
        this.studentGpa = studentGpa;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getStudentMark() {
        return studentMark;
    }

    public void setStudentMark(int studentMark) {
        this.studentMark = studentMark;
    }

    public int getStudentPosition() {
        return studentPosition;
    }

    public void setStudentPosition(int studentPosition) {
        this.studentPosition = studentPosition;
    }
}
