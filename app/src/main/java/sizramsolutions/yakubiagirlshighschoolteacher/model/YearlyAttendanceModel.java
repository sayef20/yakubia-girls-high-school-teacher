package sizramsolutions.yakubiagirlshighschoolteacher.model;



public class YearlyAttendanceModel {

    private String className;
    private int yearlyWorkingDays, totalStudent, presentStudent, absentStudent;

    public YearlyAttendanceModel(String className, int yearlyWorkingDays, int totalStudent, int presentStudent, int absentStudent){

        this.className = className;
        this.yearlyWorkingDays = yearlyWorkingDays;
        this.totalStudent = totalStudent;
        this.presentStudent = presentStudent;
        this.absentStudent = absentStudent;

    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getYearlyWorkingDays() {
        return yearlyWorkingDays;
    }

    public void setYearlyWorkingDays(int yearlyWorkingDays) {
        this.yearlyWorkingDays = yearlyWorkingDays;
    }

    public int getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(int totalStudent) {
        this.totalStudent = totalStudent;
    }

    public int getPresentStudent() {
        return presentStudent;
    }

    public void setPresentStudent(int presentStudent) {
        this.presentStudent = presentStudent;
    }

    public int getAbsentStudent() {
        return absentStudent;
    }

    public void setAbsentStudent(int absentStudent) {
        this.absentStudent = absentStudent;
    }
}
